#!/bin/bash

pdflatex thesis-sample.tex
makeindex thesis-sample.nlo -s nomencl.ist -o thesis-sample.nls
biber thesis-sample
pdflatex thesis-sample.tex
pdflatex thesis-sample.tex
